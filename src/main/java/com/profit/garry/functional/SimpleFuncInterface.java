package com.profit.garry.functional;

@FunctionalInterface
public interface SimpleFuncInterface {

    void doWork();

    String toString();
    boolean equals(Object o);
}
