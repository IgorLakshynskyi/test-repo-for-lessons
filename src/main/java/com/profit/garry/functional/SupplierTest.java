package com.profit.garry.functional;

import java.util.Scanner;
import java.util.function.Supplier;

public class SupplierTest {

    public static void main(String[] args) {

        Supplier<User> userFuctory = () -> {
            Scanner in = new Scanner(System.in);
            System.out.println("Enter name: ");
            String name = in.nextLine();
            return new User(name);
        };

        User user1 = userFuctory.get();
        User user2 = userFuctory.get();

        System.out.println("Name user1: " + user1.getName());
        System.out.println("Name user2: " + user2.getName());
    }
}
