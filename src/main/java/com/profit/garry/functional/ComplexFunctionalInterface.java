package com.profit.garry.functional;

@FunctionalInterface
public interface ComplexFunctionalInterface extends SimpleFuncInterface {

    static void doSomeWork() {
        System.out.println("Doing some work in interface impl...");
    }

    default void doSomeOtherWork() {
        System.out.println("Doing some other work in interface impl...");
    }
}
