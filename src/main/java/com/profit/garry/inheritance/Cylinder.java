package com.profit.garry.inheritance;

/**
 * Created by igor on 11.06.17.
 */
public class Cylinder extends Circle {

    /**
     * height
     */
    private double height;

    public Cylinder() {
        super();
        height = 1.0;
    }

    public Cylinder(double height) {
        super();
        this.height = height;
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    /**
     * @return
     */
    public double getHeight() {
        return height;
    }

    /**
     * @return
     */
    public double getVolume() {
        return getArea() * height;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return "Cylinder: subclass of " + super.toString()
                + "height = " + height;
    }
}
