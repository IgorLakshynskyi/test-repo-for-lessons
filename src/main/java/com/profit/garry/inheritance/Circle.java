package com.profit.garry.inheritance;

/**
 * Created by igor on 11.06.17.
 */
public class Circle {

    /**
     * radius
     */
    private double radius = 1.0;

    public Circle(double radius) {
        this.radius = radius;
    }

    /**
     *
     */
    public Circle() {
    }

    /**
     * @return
     */
    public double getRadius() {
        return radius;
    }

    /**
     * @param radius
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * @return
     */
    public double getArea() {
        return calculateArea();
    }

    /**
     * @return
     */
    private double calculateArea() {
        return this.radius * this.radius * Math.PI;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return "The radius of the circle is "
                + radius
                + "and the area is: " + calculateArea();
    }
}
