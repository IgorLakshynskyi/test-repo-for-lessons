package com.profit.garry.homework.homework1;

import static com.profit.garry.loopsandarrays.GnomeSort.gnomeSort;

import static com.profit.garry.loopsandarrays.SequentialSearch.sequentialSearch;

/**
 *
 */
public class SortAndSearch {

    /**
     *
     * @param array
     * @param target
     */
    public static void sortAndSearch(int[] array, int target) {

        gnomeSort(array);
        sequentialSearch(array, target);

    }
}

