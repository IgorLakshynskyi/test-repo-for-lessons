package com.profit.garry.homework.homework2;

import static com.profit.garry.firstapp.FirstApp.println;

/**
 * my home work
 */
public class HomeWork2 {

    /**
     * @param a
     * @param b
     */
    public static void bigger(int a, int b) {
        if (a > b) {
            println(a + " is bigger");
        } else {
            println(b + " is bigger");
        }
    }

    /**
     * @param a
     * @param b
     * @param c
     */
    public static void bigger(int a, int b, int c) {
        if (a > b && a > c) {
            println(a + " is bigger");
        } else if (b > a && b > c) {
            println(b + " is bigger");
        } else {
            println(c + " is bigger");
        }
    }

    /**
     * @param a
     * @param b
     * @param c
     * @param d
     */
    public static void bigger(int a, int b, int c, int d) {

        if (a > b && a > c && a > d) {
            println(a + " is bigger");
        } else if (b > a && b > c && b > d) {
            println(b + " is bigger");
        } else if (c > a && c > b && c > d) {
            println(c + " is bigger");
        } else {
            println(d + " is bigger");
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param a
     * @param b
     */
    public static void taskTwo(int a, int b) {
        if (a == 2 || a % 2 == 0) {
            println(a * b);
        } else {
            println(a + b);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param a
     * @param b
     */
    public static void sortTaskThree(int a, int b) {
        if (a < b) {
            println(a + " " + b);
        } else {
            println(b + " " + a);
        }

    }

    /**
     * @param a
     * @param b
     * @param c
     */
    public static void sortTaskThree(int a, int b, int c) {
        int tmp;
        if (a > b) {
            tmp = a;
            a = b;
            b = tmp;
        }
        if (b > c) {
            tmp = b;
            b = c;
            c = tmp;
        }
        if (a > b) {
            tmp = a;
            a = b;
            b = tmp;
        }
        println(a + " " + b + " " + c);


    }

    /**
     * @param a
     * @param b
     * @param c
     * @param d
     */
    public static void sortTaskThree(int a, int b, int c, int d) {
        int tmp;

        if (a > b) {
            tmp = a;
            a = b;
            b = tmp;
        }
        if (b > c) {
            tmp = b;
            b = c;
            c = tmp;
        }
        if (c > d) {
            tmp = c;
            c = d;
            d = tmp;
        }
        if (a > b) {
            tmp = a;
            a = b;
            b = tmp;
        }
        if (b > c) {
            tmp = b;
            b = c;
            c = tmp;
        }
        if (a > b) {
            tmp = a;
            a = b;
            b = tmp;
        }
        println(a + " " + b + " " + c + " " + d);
    }

}
