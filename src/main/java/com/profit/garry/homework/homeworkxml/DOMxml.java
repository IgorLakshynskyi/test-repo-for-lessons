package com.profit.garry.homework.homeworkxml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


/**
 * Created by igor on 20.06.17.
 */
public class DOMxml {
    /**
     *
     * @param args
     */
    public static void main(String[] args) {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse("people.xml");

            NodeList personList = doc.getElementsByTagName("person");
            for (int i = 0; i < personList.getLength(); i++) {
                Node p = personList.item(i);
                if (p.getNodeType() == Node.ELEMENT_NODE) {
                    Element person = (Element) p;
                    String id = person.getAttribute("id");

                    final NodeList nameList = person.getChildNodes();
                    for (int j = 0; j < nameList.getLength(); j++) {
                        Node n = nameList.item(j);
                        if (n.getNodeType() == Node.ELEMENT_NODE) {
                            Element name = (Element) n;
                            System.out.println("Person " + id + ":" + name.getTagName() + "=" + name.getTextContent());
                        }
                    }
                }

            }


        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * read data from an xml files to a string
     * @param filename
     * @return
     */
    public static String lineXml(String filename) {
        String xmlContent = "";
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                xmlContent += line;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return xmlContent;
    }

    /**
     * remove all tags from string and write data to a new text file
     * @param xmlSource
     */
    public static void newFileWOTags(String xmlSource) {
        File file = new File("new-file.txt");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            bufferedWriter.write(xmlSource.replaceAll("<[^>]*>", "")
                    .replaceAll("\\s{2,}", "\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * replace all tags and write data to a text file
     * @param xmlSource
     */
    public static void replaceTags(String xmlSource) {
        File file = new File("file-with-new-tags.txt");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            bufferedWriter.write(xmlSource.replaceAll("<[^>]*>", "<tag>")
                    .replaceAll("\\s{2,}", "\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

