package com.profit.garry.homework.homework3;

/**
 * Created by igor on 13.06.17.
 */
public class Trapezium extends Triangle {
    /**
     * magic number :)
     */
    private final double value = 0.5;
    /**
     *
     */
    private double sideD;

    public Trapezium(double sideA, double sideB, double sideC, double height, double radius, double sideD) {
        super(sideA, sideB, sideC, height, radius);
        this.sideD = sideD;
    }

    @Override
    public double calculatorArea() {
        return (getSideA() + getSideB()) * getHeight();
    }

    @Override
    public double calculatorPerimeter() {
        return super.calculatorPerimeter() + sideD;
    }

    @Override
    public String toString() {
        return "Trapezium: " + super.toString() + " The sideD is " + sideD;
    }
}
