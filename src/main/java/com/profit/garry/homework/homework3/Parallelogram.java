package com.profit.garry.homework.homework3;

/**
 * Created by igor on 13.06.17.
 */
public class Parallelogram extends Square {
    /**
     *
     */
    private double height;

    public Parallelogram(double sideA, double sideB, double height) {
        super(sideA, sideB);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public double calculatorArea() {
        return getSideB() * getHeight();
    }

    @Override
    public double calculatorPerimeter() {
        return (getSideA() + getSideB()) * 2;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
