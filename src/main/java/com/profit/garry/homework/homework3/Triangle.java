package com.profit.garry.homework.homework3;

/**
 * Created by igor on 13.06.17.
 */
public class Triangle extends Parallelogram {
    /**
     *
     */
    private double sideC;
    /**
     *
     */
    private double radius;

    public Triangle(double sideA, double sideB, double sideC, double height, double radius) {
        super(sideA, sideB, height);
        this.sideC = sideC;
        this.radius = radius;
    }

    public double getSideC() {
        return sideC;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public double calculatorArea() {
        return ((getSideA() + getSideB() +  sideC) / 2) * radius;
    }

    @Override
    public double calculatorPerimeter() {
        return getSideA() + getSideB() + sideC;
    }

    @Override
    public String toString() {
        return super.toString() + " The side B is " + getSideB();
    }
}
