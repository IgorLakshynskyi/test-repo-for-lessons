package com.profit.garry.homework.homework3;

/**
 * Created by igor on 13.06.17.
 */
public class Cube extends Square {
    /**
     * magic number :)
     */
    private final int value = 12;

    public Cube(double sideA, double sideB) {
        super(sideA, sideB);
    }

    @Override
    public double calculatorArea() {
        return Math.pow(getSideA(), getSideB());
    }

    @Override
    public double calculatorPerimeter() {
        return getSideA() * value;
    }

    @Override
    public String toString() {
        return "Cube: " + "The side A is " + getSideA() + " The side B is" + getSideB() + " The area: "
                + calculatorArea()
                + " The perimeter: " + calculatorPerimeter();
    }
}
