package com.profit.garry.homework.homework3;

/**
 * Created by igor on 13.06.17.
 */
public class Square {
    /**
     * magic number :)
     */
    private final int value = 4;
    /**
     *
     */
    private double sideA;
    /**
     *
     */
    private double sideB;

    public Square(double sideA, double sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public double getArea() {
        return calculatorArea();
    }

    public double getPerimeter() {
        return calculatorPerimeter();
    }

    /**
     *
     * @return
     */
    public double calculatorArea() {
        return Math.pow(sideA, sideB);
    }

    /**
     *
     * @return
     */
    public double calculatorPerimeter() {
        return sideA * value;
    }

    /**
     *
     * @return
     */
    public String toString() {
        return "The side A is " + sideA + " The side B is " + sideB + " The area: " + getArea()
                + " The perimeter: " + getPerimeter();
    }
}
