package com.profit.garry.homework.homework3;

/**
 * Created by igor on 13.06.17.
 */
public class Circle {
    /**
     *
     */
    private double radius;
    /**
     *
     */
    private double area;
    /**
     *
     */
    private double perimeter;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setArea(double area) {
        this.area = area;
    }


    public double getArea() {
        return calculatorArea();
    }

    public double getPerimeter() {
        return calculatorPerimeter();
    }

    /**
     *
     * @return
     */
    public double calculatorArea() {
        return Math.pow(radius, radius) * Math.PI;
    }

    /**
     *
     * @return
     */
    public double calculatorPerimeter() {
        return 2 * Math.PI * radius;
    }

    /**
     *
     * @return
     */
    public String toString() {
        return "The radius is " + getRadius() + " The area: " + getArea()
                + " The perimeter: " + getPerimeter();
    }
}
