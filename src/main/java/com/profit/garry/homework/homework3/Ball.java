package com.profit.garry.homework.homework3;

/**
 * Created by igor on 13.06.17.
 */
public class Ball extends Circle {
    /**
     * magic number :)
     */
    private final int value = 4;

    public Ball(double radius) {
        super(radius);
    }

    @Override
    public void setArea(double area) {
        super.setArea(area);
    }


    @Override
    public double getArea() {
        return super.getArea() * value;
    }

    @Override
    public double getPerimeter() {
        return super.getPerimeter();
    }

    @Override
    public double calculatorArea() {
        return super.calculatorArea();
    }

    @Override
    public double calculatorPerimeter() {
        return super.calculatorPerimeter();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
