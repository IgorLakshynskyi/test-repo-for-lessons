package com.profit.garry.interfaces;

import java.util.Comparator;

/**
 * Created by igor on 18.06.17.
 */
public class Fruit implements Comparable<Fruit> {
    /**
     *
     */
    private String fruitName;
    /**
     *
     */
    private String fruitDesc;
    /**
     *
     */
    private int quantity;

    protected Fruit(String fruitName, String fruitDesc, int quantity) {
        super();
        this.fruitName = fruitName;
        this.fruitDesc = fruitDesc;
        this.quantity = quantity;
    }

    public String getFruitName() {
        return fruitName;
    }

    public void setFruitName(String fruitName) {
        this.fruitName = fruitName;
    }

    public String getFruitDesc() {
        return fruitDesc;
    }

    public void setFruitDesc(String fruitDesc) {
        this.fruitDesc = fruitDesc;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public int compareTo(Fruit compareFruit) {
        int compareQuantity = ((Fruit) compareFruit).getQuantity();

        return this.quantity - compareQuantity;
    }

    /**
     *
     */
    protected static Comparator<Fruit> fruitNameComparator =
            new Comparator<Fruit>() {
                @Override
                public int compare(Fruit fruitl, Fruit fruitS) {

                    String fruitNamel = fruitl.getFruitName().toUpperCase();
                    String fruitNameS = fruitS.getFruitName().toUpperCase();

                    return fruitNamel.compareTo(fruitNameS);
                }
            };
}
