package com.profit.garry.javanet;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ThreadController implements Runnable {

    private String url;
    private int counter;
    private Path path = Paths.get(".").toAbsolutePath().getParent();

    public ThreadController(String url) {
        this.url = url;
    }

    @Override
    public void run() {
        String link;

        try {
            File linksFile = new File(path + "/src/main/resources/links",
                    Thread.currentThread().getName() + ".html");
            try (BufferedWriter linksFileWriter = new BufferedWriter(new FileWriter(linksFile, true))) {
                Connection.Response html = Jsoup.connect(url).execute();
                linksFileWriter.write(html.body());

                System.out.println(Thread.currentThread().getName() + " Started");

                counter = TestMain.atomicInteger.incrementAndGet();
                System.out.println(counter + " number of Sites are checked!");
            }
        } catch (IOException e) {
            e.printStackTrace();

        }
    }
}
