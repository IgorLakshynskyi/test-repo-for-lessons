package com.profit.garry.inner;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by igor on 01.07.17.
 */
public class Cache {

    private Map<String, CacheEntry> cacheMap = new HashMap<>();


    private class CacheEntry {
        private Long timeInsert = 0L;
        private Object value = null;
    }

    /**
     * @param key
     * @param value
     */
    public void store(String key, Object value) {
        CacheEntry entry = new CacheEntry();
        entry.value = value;
        entry.timeInsert = System.currentTimeMillis();
        this.cacheMap.put(key, entry);
    }

    /**
     * @param key
     * @return
     */
    public Object get(String key) {
        CacheEntry entry = this.cacheMap.get(key);
        if (entry == null) {
            return null;
        }
        return entry.value;
    }

    /**
     * @param key
     * @return
     */
    public String getData(String key) {
        CacheEntry entry = this.cacheMap.get(key);
        if (entry == null) {
            return null;
        }
        return entry.value.toString().concat(" ").concat(entry.timeInsert.toString());
    }
}
