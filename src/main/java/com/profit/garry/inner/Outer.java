package com.profit.garry.inner;

/**
 * Created by igor on 01.07.17.
 */
public class Outer {

    private String text = "I'm Outer private!";
    private static String textNested = "I'm Nested";


    public static class Nested {
        /**
         *
         */
        public void printNestedText() {
            System.out.println(textNested);
        }

    }

    public class Inner {

        private String text = "I'm Inner private!";

        /**
         *
         */
        public void printText() {
            System.out.println(text);
            System.out.println(Outer.this.text);
        }
    }

    /**
     *
     */
    public void localClass() {

        class Local {
        }

        Local local = new Local();
    }

    /**
     *
     */
    public void doIT() {
        System.out.println("Outer class doIT()");
    }
}
