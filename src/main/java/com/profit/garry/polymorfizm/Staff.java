package com.profit.garry.polymorfizm;

import java.util.ArrayList;

/**
 * Created by igor on 17.06.17.
 */
public class Staff {
    /**
     *
     */
    private ArrayList<StaffMember> staffList;

    /**
     *
     */
    public Staff() {
        staffList = new ArrayList<>();
        staffList.add(new Executive("Carla", "Carla_address", "234-546", "45645", 354.4));
        staffList.add(new Employee("Woody", "Woody_address", "746-33", "7564", 6.67));
        staffList.add(new Hourly("Diane", "Diane_address", "45-34-23", "1234", 45.234));
        staffList.add(new Volunteer("Norm", "Norm_address", "23-45-56-67"));
        staffList.add(new Volunteer("Cliff", "Cliff_address", "12-12-12"));

        ((Executive) staffList.get(0)).awardBonus(500.00);
        ((Hourly) staffList.get(2)).addHours(40);
    }

    /**
     *
     */
    public void payday() {
        double amount;
        for (int count = 0; count < staffList.size(); count++) {
            System.out.println(staffList.get(count));
            amount = staffList.get(count).pay();
            //polymorphic
            if (amount == 0) {
                System.out.println("Thanks");
            } else {
                System.out.println("Paid: " + amount);
            }
            System.out.println("--------------------------");
        }
    }
}
