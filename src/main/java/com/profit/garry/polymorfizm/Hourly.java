package com.profit.garry.polymorfizm;

/**
 * Created by igor on 17.06.17.
 */
public class Hourly extends Employee {
    /**
     *
     */
    private int hoursWorked;

    /**
     * @param eName
     * @param eAddress
     * @param ePhone
     * @param socialSecurityNumber
     * @param rate

     */
    public Hourly(String eName, String eAddress, String ePhone, String socialSecurityNumber,
                  double rate) {
        super(eName, eAddress, ePhone, socialSecurityNumber, rate);
        this.hoursWorked = 0;
    }

    /**
     * @param moreHours
     */
    public void addHours(int moreHours) {
        hoursWorked += moreHours;
    }

    /**
     * @return
     */
    @Override
    public double pay() {
        double payment = payRate * hoursWorked;
        hoursWorked = 0;
        return payment;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        String result = super.toString();
        result += "\nCurrent hours: " + hoursWorked;
        return result;
    }
}
