package com.profit.garry.polymorfizm;

/**
 * Created by igor on 17.06.17.
 */
public class Employee extends StaffMember {
    /**
     * socialSecurityNumber
     */
    protected String socialSecurityNumber;
    /**
     * payRate
     */
    protected double payRate;

    /**
     * constructor
     * @param eName
     * @param eAddress
     * @param ePhone
     * @param socialSecurityNumber
     * @param rate
     */
    public Employee(String eName, String eAddress, String ePhone, String socialSecurityNumber, double rate) {
        super(eName, eAddress, ePhone);
        this.socialSecurityNumber = socialSecurityNumber;
        payRate = rate;
    }

    /**
     * return information toString
     * @return
     */
    public String toString() {
        String result = super.toString();
        result += "\nSocial Securety Number: " + socialSecurityNumber;
        return result;
    }

    /**
     * return payRate
     *
     * @return
     */
    @Override
    public double pay() {
        return payRate;
    }
}
