package com.profit.garry.polymorfizm;

/**
 * Created by igor on 17.06.17.
 */
public class Volunteer extends StaffMember {
    /**
     * @param eName
     * @param eAddress
     * @param ePhone
     */
    public Volunteer(String eName, String eAddress, String ePhone) {
        super(eName, eAddress, ePhone);
    }

    /**
     * @return
     */
    @Override
    public double pay() {
        return 0.0;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return super.toString();
    }
}
