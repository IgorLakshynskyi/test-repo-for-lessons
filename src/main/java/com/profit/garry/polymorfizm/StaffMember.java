package com.profit.garry.polymorfizm;

/**
 * Created by igor on 17.06.17.
 */
abstract class StaffMember {
    /**
     * name
     */
    private String name;
    /**
     * address
     */
    private String address;
    /**
     * phone
     */
    private String phone;

    /**
     * @param eName
     * @param eAddress
     * @param ePhone
     */
    protected StaffMember(String eName, String eAddress, String ePhone) {
        name = eName;
        address = eAddress;
        phone = ePhone;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        String result = "Name: " + name + "\n";
        result += "Address: " + address + "\n";
        result += "Phone " + phone + "\n";
        return result;
    }

    /**
     * @return
     */
    public abstract double pay();
}
