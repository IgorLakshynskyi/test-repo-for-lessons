package com.profit.garry.loopsandarrays;

/**
 * Created by igor on 04.06.17.
 */
public class Sorting {
    /**
     * 2 1 4 3
     *
     * @param num
     */
    public static void selectionSort(int[] num) {

        int first;
        int temp;

        for (int i = num.length - 1; i > 0; i--) {
            //initialize to subscription of first element
            first = 0;

            //locate smallest element between position 1 and i.
            for (int j = 1; j <= i; j++) {

                if (num[j] < num[first]) {
                    first = j;
                }

                //swap smallest found with element in position 1.
                temp = num[first];
                num[first] = num[i];
                num[i] = temp;

            }
        }
    }
}
