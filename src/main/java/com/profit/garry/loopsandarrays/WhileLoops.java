package com.profit.garry.loopsandarrays;

import static com.profit.garry.firstapp.FirstApp.print;

import static com.profit.garry.firstapp.FirstApp.println;


/**
 * Created by igor on 01.06.17.
 */
public class WhileLoops {

    /**
     *
     * @param str
     * @param again
     */
    public static void selectChoice(String str, String again) {

        String choice;
        String con = "y";

        println("What is the command keyboard to exit a loop in Java?");

        println("a.quit");
        println("b.continue");
        println("c.break");
        println("d.exit");


        while (con.compareTo("y") == 0) {
            print("Enter your choice:");

            choice = str;

            if (choice.compareTo("c") == 0) {
                print("Congratulation!");
                break;
            } else if (choice.compareTo("q") == 0 || choice.compareTo("e") == 0) {
                print("Exiting...!");
                break;
            } else {
                print("Incorrect!");
                break;

            }
        }
    }
}
