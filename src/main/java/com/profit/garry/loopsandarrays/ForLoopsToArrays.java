package com.profit.garry.loopsandarrays;

import java.util.Arrays;

/**
 * Created by igor on 03.06.17.
 */
public class ForLoopsToArrays {

    /**
     * @param myArrayl
     * @param myArrayS
     */
    public static void equalityOfTwoArrays(int[] myArrayl, int[] myArrayS) {
        boolean equalOrNot = true;

        if (myArrayl.length == myArrayS.length) {

            for (int i = 0; i < myArrayl.length; i++) {
                if (myArrayl[i] != myArrayS[i]) {
                    equalOrNot = false;
                }
            }

        } else {
            equalOrNot = false;
        }

        if (equalOrNot) {
            System.out.println("Two arrays are eaqul.");
        } else {
            System.out.println("Two arrays are not equal.");
        }
    }

    /**
     * @param myArray
     */
    public static void uniqueArray(int[] myArray) {

        int arraySize = myArray.length;

        System.out.println("Original Array : ");

        for (int i = 0; i < arraySize; i++) {

            System.out.println(myArray[i] + "\t");
        }

        //Comparing each element with all other elements
        for (int k = 0; k < arraySize; k++) {

            for (int j = k + 1; j < arraySize; j++) {

                //IF any two elements are found eaqual
                if (myArray[k] == myArray[j]) {

                    //Replace duplicate element with last unique element
                    myArray[j] = myArray[arraySize - 1];

                    arraySize--;

                    j--;
                }
            }
        }


        //Copying only unique elements of my_array into arrayK

        int[] arrayK = Arrays.copyOf(myArray, arraySize);

        //Printing arrayWithoutDuplicates

        System.out.println();
        System.out.println("Array with unique values : ");

        for (int i = 0; i < arrayK.length; i++) {
            System.out.println(arrayK[i] + "\t");
        }
    }
}




