package com.profit.garry.loopsandarrays;

/**
 * Created by igor on 08.06.17.
 */
public class SequentialSearch {

    /**
     *
     * @param arr
     * @param target
     */
    public static void sequentialSearch(int[] arr, int target) {

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == target) {
                System.out.println("value " + arr[i] + " -- " + (i + 1) + "-th in list");
                break;
            } else if (i == arr.length - 1 && arr[i] != target) {
                System.out.println("Target wasn't been Found");
            }
        }
    }
}
