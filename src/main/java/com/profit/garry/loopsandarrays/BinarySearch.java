package com.profit.garry.loopsandarrays;


/**
 * Created by igor on 08.06.17.
 */
public class BinarySearch {
    /**
     *
     * @param array
     * @param target
     */
    public static void binarySearch(int[] array, int target) {

        int low = 0;
        int high = array.length - 1;

        int middle = 0;
        while (high >= low) {
             middle = (low + high) / 2;
            if (array[middle] == target) {
                System.out.println(array[middle]);
                break;
            }
            if (array[middle] < target) {
                low = middle + 1;
            }
            if (array[middle] > target) {
                high = middle - 1;
            }

        }
        if (array[middle] != target) {
            System.out.println("UnFound");
        }
    }
}
