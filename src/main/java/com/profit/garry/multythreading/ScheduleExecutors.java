package com.profit.garry.multythreading;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ScheduleExecutors {

    public static void main(String[] args) {

//        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
//
//        Runnable task = () -> System.out.println("Scheduling: " + System.currentTimeMillis());
//        ScheduledFuture<?> future = executorService.schedule(task, 3, TimeUnit.SECONDS);
//
//        try {
//            TimeUnit.MILLISECONDS.sleep(1337);
//            long remainingDelay = future.getDelay(TimeUnit.MILLISECONDS);
//            System.out.printf("Remaining Delay: %s", remainingDelay);
//            System.out.println();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }finally {
//            executorService.shutdownNow();
//        }


        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

        Runnable task = () -> {
            try {
                TimeUnit.SECONDS.sleep(2);
                System.out.println("Scheduling: " + System.currentTimeMillis());
            } catch (InterruptedException e) {
                System.err.println("task interrupted");
            }finally {
                executor.shutdownNow();
            }
        };

        executor.scheduleWithFixedDelay(task, 0, 1, TimeUnit.SECONDS);
    }
}
