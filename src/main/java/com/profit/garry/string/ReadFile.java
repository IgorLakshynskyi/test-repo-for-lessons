package com.profit.garry.string;

import java.io.BufferedReader;

import java.io.FileNotFoundException;

import java.io.FileReader;

import java.io.IOException;

import java.util.StringTokenizer;

/**
 * Created by igor on 10.06.17.
 */
public class ReadFile {
    /**
     *
     * @param path
     */
    public static void readCSV(String path) {


        String line;
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {

            while ((line = br.readLine())  != null) {
                System.out.println(line);

                StringTokenizer stringTokenizer = new StringTokenizer(line, "|");

                while (stringTokenizer.hasMoreElements()) {
                    Integer id = Integer.parseInt(stringTokenizer.nextElement().toString());
                    Double price = Double.parseDouble(stringTokenizer.nextElement().toString());
                    String userName = stringTokenizer.nextElement().toString();

                    StringBuilder sb = new StringBuilder();
                    sb.append("\nID :" + id);
                    sb.append("\nPrice :" + price);
                    sb.append("\nUserName :" + userName);
                    sb.append("\n************\n");

                    System.out.println(sb.toString());
                }
            }
            System.out.println("Done");
        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        }
    }
}
