package com.profit.garry.string;

/**
 * Created by igor on 10.06.17.
 */
public class SubString {
    /**
     *
     * @param str
     * @return
     */
    public String subStringSearch(String str) {
        StringBuilder result = new StringBuilder();

        int length = str.length();
        for (int i = 0; i < length / 2; i++) {
            char symbol = str.charAt(i);
            if (symbol == str.charAt(length - (i + 1))) {
                result.append(symbol);
            } else {
                break;
            }
        }
        return result.toString();
    }
}
