package com.profit.garry.string;

import java.util.StringTokenizer;


/**
 * Created by igor on 10.06.17.
 */
public class StringTokenizing {
    /**
     * @param str
     */
    public static void splitBySpace(String str) {
        StringTokenizer st = new StringTokenizer(str);

        System.out.println("_ _ _ _ Split by Space _ _ _ _");
        while (st.hasMoreElements()) {
            System.out.println(st.nextElement());
        }
    }

    /**
     * @param str
     */
    public static void splitByComa(String str) {
        System.out.println("_ _ _ _ Split by Coma ','  _ _ _ _");
        StringTokenizer stSecond = new StringTokenizer(str, ",", true);

        while (stSecond.hasMoreElements()) {
            System.out.println(stSecond.nextElement());
        }
    }
}
