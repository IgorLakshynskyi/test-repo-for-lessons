package com.profit.garry.pattern.abstraktFactory;

/**
 * Created by igor on 15.07.17.
 */
public class AbstraktFactory {

    public SpeciesFactory getSpeciesFactory(String type) {
        if ("mammal".equals(type)) {
            return new MammalFactory();
        } else {
            return new ReptileFactory();
        }
    }
}
