package com.profit.garry.pattern.abstraktFactory;

import com.profit.garry.pattern.factory.Animal;
import com.profit.garry.pattern.factory.Cat;
import com.profit.garry.pattern.factory.Dog;

/**
 * Created by igor on 15.07.17.
 */
public class MammalFactory extends SpeciesFactory {

    @Override
    public Animal getAnimal(String type) {
        if ("dog".equals(type)) {
            return new Dog();
        } else {
            return new Cat();
        }
    }
}
