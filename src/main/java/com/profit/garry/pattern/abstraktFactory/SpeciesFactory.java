package com.profit.garry.pattern.abstraktFactory;

import com.profit.garry.pattern.factory.Animal;

/**
 * Created by igor on 15.07.17.
 */
public abstract class SpeciesFactory {

    public abstract Animal getAnimal(String type);
}
