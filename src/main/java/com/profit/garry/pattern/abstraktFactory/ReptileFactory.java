package com.profit.garry.pattern.abstraktFactory;

import com.profit.garry.pattern.factory.Animal;
import com.profit.garry.pattern.factory.Snuke;
import com.profit.garry.pattern.factory.Tyranosaurus;

/**
 * Created by igor on 15.07.17.
 */
public class ReptileFactory extends SpeciesFactory {

    @Override
    public Animal getAnimal(String type) {
        if ("snake".equals(type)) {
            return new Snuke();
        } else {
            return new Tyranosaurus();
        }
    }
}
