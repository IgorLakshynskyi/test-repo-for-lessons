package com.profit.garry.pattern.builder;

/**
 * Created by igor on 15.07.17.
 */
public interface MealBuilder {

    public void buildDrink();

    public void buildMainCourse();

    public void buildSide();

    public Meal getMeal();
}
