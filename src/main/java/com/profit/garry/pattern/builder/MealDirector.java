package com.profit.garry.pattern.builder;

/**
 * Created by igor on 15.07.17.
 */
public class MealDirector {

    private MealBuilder mealBuilder = null;

    public MealDirector(MealBuilder mealBuilder) {
        this.mealBuilder = mealBuilder;
    }

    /**
     *
     * @return
     */
    public MealDirector constructMeal() {
        mealBuilder.buildDrink();
        mealBuilder.buildMainCourse();
        mealBuilder.buildSide();
        return this;
    }

    public Meal getMeal() {
        return mealBuilder.getMeal();
    }
}
