package com.profit.garry.pattern.builder;

/**
 * Created by igor on 15.07.17.
 */
public class JapaneseMealBuilder implements MealBuilder {

    private Meal meal;

    public JapaneseMealBuilder() {
        meal = new Meal();
    }

    @Override
    public void buildDrink() {
        meal.setDrink("sake");
    }

    @Override
    public void buildMainCourse() {
        meal.setMainCourse("chicken teriyaki");

    }

    @Override
    public void buildSide() {
        meal.setSide("miso Soup");
    }

    @Override
    public Meal getMeal() {
        return meal;

    }
}
