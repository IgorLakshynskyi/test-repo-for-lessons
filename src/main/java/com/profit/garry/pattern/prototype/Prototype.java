package com.profit.garry.pattern.prototype;

/**
 * Created by igor on 15.07.17.
 */
public interface Prototype {
    /**
     *
     * @return
     */
    public Prototype doClone();
}
