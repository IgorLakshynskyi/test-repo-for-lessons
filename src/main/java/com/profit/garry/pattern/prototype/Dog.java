package com.profit.garry.pattern.prototype;

/**
 * Created by igor on 15.07.17.
 */
public class Dog implements Prototype {

    private String sound;

    public Dog(String sound) {
        this.sound = sound;
    }

    @Override
    public Prototype doClone() {
        return new Dog(sound);
    }

    /**
     *
     * @return
     */
    public String toString() {
        return "This dog says " + sound;
    }
}
