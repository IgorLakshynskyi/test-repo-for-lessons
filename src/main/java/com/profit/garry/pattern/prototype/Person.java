package com.profit.garry.pattern.prototype;

/**
 * Created by igor on 15.07.17.
 */
public class Person implements Prototype {

    private String name;

    public Person(String name) {
        this.name = name;
    }

    @Override
    public Prototype doClone() {
        return new Person(name);
    }

    /**
     *
     * @return
     */
    public String toString() {
        return "This person is named" + name;
    }
}
