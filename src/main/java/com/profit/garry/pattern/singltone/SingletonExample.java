package com.profit.garry.pattern.singltone;

/**
 * Created by igor on 15.07.17.
 */
public class SingletonExample {

    private static SingletonExample singletonExample = null;

    private SingletonExample() {

    }

    /**
     *
     * @return
     */
    public static SingletonExample getInstance() {
        if (singletonExample == null) {
            singletonExample = new SingletonExample();
        }
        return singletonExample;
    }

    /**
     *
     */
    public void sayHello() {
        System.out.println("Hello");
    }
}
