package com.profit.garry.pattern.factory;

/**
 * Created by igor on 15.07.17.
 */
public abstract class Animal {

    public abstract String makeSound();
}
