package com.profit.garry.pattern.factory;

/**
 * Created by igor on 15.07.17.
 */
public class Tyranosaurus extends Animal {
    @Override
    public String makeSound() {
        return "Grroyrfff";
    }
}
