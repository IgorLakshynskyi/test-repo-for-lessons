package com.profit.garry.pattern.factory;

/**
 * Created by igor on 15.07.17.
 */
public class Cat extends Animal{
    @Override
    public String makeSound() {
        return "May:)";
    }
}
