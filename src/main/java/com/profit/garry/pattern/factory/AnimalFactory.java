package com.profit.garry.pattern.factory;

/**
 * Created by igor on 15.07.17.
 */
public class AnimalFactory {

    public Animal getAnimals(String type) {
        if ("canine".equals(type)) {
            return new Dog();
        }else {
            return new Cat();
        }
    }
}
