package com.profit.garry.generic;

public abstract class Animal<T> {

    public abstract T nameT(T t);

    public abstract T getData();

    public void text() {
        System.out.println("This is Animal");
    }
}
