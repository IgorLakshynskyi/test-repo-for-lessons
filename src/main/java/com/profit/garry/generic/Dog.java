package com.profit.garry.generic;

public class Dog extends Animal {

    private String dogName;

    @Override
    public Dog nameT(Object o) {
        this.dogName = (String) o;
        return this;
    }

    @Override
   public Object getData() {
        return dogName;
    }
}
