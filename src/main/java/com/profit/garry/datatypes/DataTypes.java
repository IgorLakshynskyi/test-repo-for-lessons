package com.profit.garry.datatypes;

/**
 * Created by igor on 29.05.17.
 */
public class DataTypes {

    /**
     * this is a byte
     */

    private byte aByte;
    /**
     *
     */
    private short aShort;
    /**
     *
     */
    private int anInt;
    /**
     *
     */
    private long aLong;
    /**
     *
     */
    private double aDouble;
    /**
     *
     */
    private float aFloat;
    /**
     *
     */
    private char aChar;
    /**
     *
     */
    private boolean aBoolean;

    public DataTypes(byte aByte, short aShort, int anInt,
                     long aLong, double aDouble, float aFloat,
                     char aChar, boolean aBoolean) {
        this.aByte = aByte;
        this.aShort = aShort;
        this.anInt = anInt;
        this.aLong = aLong;
        this.aDouble = aDouble;
        this.aFloat = aFloat;
        this.aChar = aChar;
        this.aBoolean = aBoolean;
    }

    /**
     * return byte
     *
     * @return
     */

    public byte getaByte() {
        return aByte;
    }

    /**
     * return short
     *
     * @return
     */

    public short getaShort() {
        return aShort;
    }

    /**
     * return int
     *
     * @return
     */

    public int getAnInt() {
        return anInt;
    }

    /**
     * return long
     *
     * @return
     */

    public long getaLong() {
        return aLong;
    }

    /**
     * return double
     *
     * @return
     */

    public double getaDouble() {
        return aDouble;
    }

    /**
     * @return
     */

    public float getaFloat() {
        return aFloat;
    }

    /**
     * @return
     */

    public char getaChar() {
        return aChar;
    }

    /**
     * @return
     */

    public boolean getaBoolean() {
        return aBoolean;
    }

    /**
     * @return falseor true
     */
}
