package com.profit.garry.operators;

/**
 * Created by igor on 18.06.17.
 */
public class TernaryOperator {
    /**
     *
     */
    private Integer first;
    /**
     *
     */
    private Integer second;

    public TernaryOperator(Integer first, Integer second) {
        this.first = first;
        this.second = second;
    }

    public TernaryOperator() {
    }

    public Integer getFirst() {
        return first;
    }

    public Integer getSecond() {
        return second;
    }

    /**
     *
     * @param i
     * @param j
     * @return
     */
    public static int getMainValue(int i, int j) {
        return (i < j) ? i : j;
    }

    /**
     *
     * @param i
     * @return
     */
    public static int getAbsoluteValue(int i) {
        return i < 0 ? -i : i;
    }

    /**
     *
     * @param b
     * @return
     */
    public static boolean invertBoolean(boolean b) {
        return b ? false : true;
    }

    /**
     *
     * @param str
     */
    public static void containsA(String str) {
        String data = str.contains("A") ? "STR cont" : "STR don't cont";
    }

    /**
     *
     * @param x
     * @param ternaryOperator
     */
    public static void ternaryMethod(Integer x, TernaryOperator ternaryOperator) {

        System.out.println((x.equals(ternaryOperator.getFirst()))
                ? "i = 5" : ((x.equals(ternaryOperator.getSecond())) ? "i = 10" : "i is not equal to 5 or 10"));
    }
}
