package com.profit.garry.firstapp;

import static java.lang.System.*;

/**
 * Created by igor on 29.05.17.
 */
public class FirstApp {

    /**
     * print method
     * @param t
     * @param <T>
     */

    public static <T> void print(T t) {
        out.print(t);
    }

    /**
     * print method that print object
     * @param object
     */

    public static void println(Object object) {
        out.println(object);
    }

    /**
     * print method that print new line
     */

    public static void println() {
        out.println();
    }
}
