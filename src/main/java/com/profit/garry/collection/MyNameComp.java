package com.profit.garry.collection;

import java.util.Comparator;

/**
 * Created by igor on 09.07.17.
 */
public class MyNameComp implements Comparator<Empl> {

    @Override
    public int compare(Empl empl, Empl tI) {
        return empl.getName().compareTo(tI.getName());
    }
}
