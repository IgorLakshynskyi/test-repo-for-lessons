package com.profit.garry.collection;

import java.util.Comparator;

/**
 * Created by igor on 09.07.17.
 */
public class MySalaryComp implements Comparator<Empl> {

    @Override
    public int compare(Empl empl, Empl tI) {

        if (empl.getSalary() > tI.getSalary()) {
            return 1;
        } else {
            return -1;
        }
    }
}
