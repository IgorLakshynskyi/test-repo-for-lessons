package com.profit.garry.enums;

public class StatusExampleSecond {

    public enum Status {
        STATUS_OPEN(0, "OPEN"),
        STATUS_STARTED(1, "started"),
        STATUS_INPROGRESS(2, "inprogres"),
        STATUS_ONHOLD(3, "onhold"),
        STATUS_COMPLETED(4, "completed"),
        STATUS_CLOSED(5, "closed");

        private final int status;
        private final String description;

        Status(int status, String description) {
            this.status = status;
            this.description = description;
        }

        public int status() {
            return this.status;
        }

        public String description() {
            return this.description;
        }
    }

    public static void main(String[] args) {
        for (Status stat : Status.values()) {
            System.out.println(stat + "value is " + new Integer(stat.status) + " task is " + stat.description);

        }
    }
}
