package com.profit.garry.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.profit.garry.firstapp.FirstApp.println;

public class StatusExampleAbstract {

    public enum Status {

        STATUS_OPEN {
            public String description() {
                return "open";
            }
        },
        STATUS_STARTED

                {
                    public String description() {
                        return "started";
                    }
                },
        STATUS_INPROGRESS

                {
                    public String description() {
                        return "inprogress";
                    }
                },
        STATUS_ONHOLD

                {
                    public String description() {
                        return "onhold";
                    }
                },
        STATUS_COMPLETED

                {
                    public String description() {
                        return "completed";
                    }
                },
        STATUS_CLOSED {
            public String description() {
                return "closed";
            }
        };

        Status() {
        }

        public abstract String description();
    }

    public static void main(String[] args) {

        List<Status> enums = new ArrayList<>();
        for (Status stat : Status.values()) {
            enums.add(stat);
        }

//        enums.forEach(x -> { println(x.description());
//        });


        List<Status> result = enums.stream().collect(Collectors.toList());
        result.forEach(System.out::println);

    }
}