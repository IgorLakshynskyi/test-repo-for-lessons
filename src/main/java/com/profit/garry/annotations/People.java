package com.profit.garry.annotations;

import java.io.Serializable;

/**
 * Created by igor on 08.07.17.
 */
public class People implements Serializable, Cloneable {

    private String name;

    private int age;

    protected int sum;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Deprecated
    protected static void method(String[] params) {

    }
}
