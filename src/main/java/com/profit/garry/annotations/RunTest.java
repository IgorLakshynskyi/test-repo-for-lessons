package com.profit.garry.annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Created by igor on 08.07.17.
 */
public class RunTest {
    /**
     *
     * @param args
     */
    public static void main(String[] args) {

        System.out.println("Testing...");

        int passed = 0;
        int failed = 0;
        int count = 0;
        int ignore = 0;

        Class<AnotationTest> obj = AnotationTest.class;

        if (obj.isAnnotationPresent(TestInfo.class)) {
            Annotation annotation = obj.getAnnotation(TestInfo.class);
            TestInfo testInfo = (TestInfo) annotation;

            System.out.printf("%nPriority : %s", testInfo.priority());
            System.out.printf("%nCreateBy : %s", testInfo.createBy());
            System.out.printf("%nTags : ");

            int tagLength = testInfo.tags().length;
            for (String tag : testInfo.tags()) {
                if (tagLength > 1) {
                    System.out.println(tag + ", ");
                } else {
                    System.out.println(tag);
                }
                tagLength--;

            }
            System.out.printf("%nLastModified : %s%n%n", testInfo.lastModified());
        }

        for (Method method : obj.getDeclaredMethods()) {
            if (method.isAnnotationPresent(Test.class)) {

                Annotation annotation = method.getAnnotation(Test.class);
                Test test = (Test) annotation;

                if (test.enabled()) {
                    try {
                        method.invoke(obj.newInstance());
                        System.out.printf("%s - Test '%s' - passed %n", ++count, method.getName());
                        passed++;
                    } catch (Throwable ex) {
                        System.out.printf("%s - Test '%s' - failed: %s %n", ++count, method.getName(), ex.getCause());
                        failed++;
                    }
                } else {
                    System.out.printf("%s - Test %s - ignored%n", ++count, method.getName());
                    ignore++;
                }
            }
        }

        System.out.printf("%nResult : Total : %d, Failed %d, Ignore %d%n", passed, failed, ignore);
        System.out.println(count);
    }
}
