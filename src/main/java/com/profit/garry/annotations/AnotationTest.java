package com.profit.garry.annotations;

/**
 * Created by igor on 08.07.17.
 */

@TestInfo(
        priority = TestInfo.Priority.HIGH,
        createBy = "andrey",
        tags = {"sales", "test"}
)
public class AnotationTest {

    @Test
    void testA() {
        if (true) {
            throw new RuntimeException("This test always failed");
        }
    }

    @Test(enabled = false)
    void testB() {
        if (false) {
            throw new RuntimeException("This test always passed");
        }

    }

    @Test(enabled = true)
    void testC() {
        if (10 > 1) {
            System.out.println();
        }

    }
}
