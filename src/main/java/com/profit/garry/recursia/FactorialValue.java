package com.profit.garry.recursia;

/**
 * Created by igor on 22.06.17.
 */
public class FactorialValue {
    /**
     * @param x
     * @return
     */
    public static int factorial(int x) {

        if (x == 0 || x < 0 || x == 1) {
            return 1;
        } else {

            return x * factorial(x - 1);
        }
    }
}
