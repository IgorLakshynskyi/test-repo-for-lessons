package com.profit.garry.encapsulation;

import java.util.Date;

/**
 * Created by igor on 17.06.17.
 */
public class User {
    /**
     *
     */
    private String firstName;
    /**
     *
     */
    private String lastName;
    /**
     *
     */
    private String loginID;
    /**
     *
     */
    private String password;
    /**
     *
     */
    private String email;
    /**
     *
     */
    private Date registered;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLoginID() {
        return loginID;
    }

    public void setLoginID(String loginID) {
        this.loginID = loginID;
    }

    /**
     * @param loginID
     */
    public void setLoginIDSecond(String loginID) {
        if (loginID.matches("^[a-zA-Z][a-zA-Z0-9]*$")) {
            this.loginID = loginID;
        }
    }

    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        if (password.length() < 6) {
            System.err.println("Password is too short");
        } else {
            this.password = password;
        }
    }

    /**
     *
     * @param password
     */
    public void setPasswordNull(String password) {
        if (password == null || password.length() < 6) {
            System.err.println("Password is too short");
        } else {
            this.password = password;
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getRegistered() {
        return registered;
    }

    public void setRegistered(Date registered) {
        this.registered = registered;
    }
}
