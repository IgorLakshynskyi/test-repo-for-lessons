package com.profit.garry.logger;

import org.apache.log4j.Logger;

public class LoggerExample {

    final static Logger logger = Logger.getLogger(LoggerExample.class);

    public static void main(String[] args) {

        LoggerExample obj = new LoggerExample();
        obj.runMe("DaviD");

        try {
            obj.divide();
        }catch (ArithmeticException ex) {
            logger.error("Sorry, smth wrong:)", ex);
        }
    }

    private void runMe(String parameter) {

        logger.debug("This is debug : " + parameter);
        logger.info("This is info : " + parameter);
        logger.warn("This is warn : " + parameter);
        logger.error("This is error : " + parameter);
        logger.fatal("This is fatal : " + parameter);
    }

    private void divide() {
        int i = 10/0;
    }
}
