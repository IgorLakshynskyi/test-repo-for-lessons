package com.profit.garry.conditional;

/**
 * Created by igor on 29.05.17.
 */
public class ConditionalStatements {

    protected ConditionalStatements() {
    }

    /**
     * @param x
     */

    public static void positivOrNegativ(Integer x) {

        int input = x;

        if (input > 0) {
            System.out.println("Number is positive");
        } else if (input < 0) {
            System.out.println("Number is negativ");
        } else {
            System.out.println("Number zero");
        }
    }
}
