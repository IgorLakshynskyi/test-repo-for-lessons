package com.profit.garry.string;

import org.junit.Test;

/**
 * Created by igor on 10.06.17.
 */
public class TestStringTokenizing {

    @Test
    public void testingSplitBySpace() {
        String str = "abc dfg SJH";
        StringTokenizing.splitBySpace(str);
    }

    @Test
    public void testingSplitByComa() {
        String str = "abc,dfg,SJH";
        StringTokenizing.splitByComa(str);
    }
}
