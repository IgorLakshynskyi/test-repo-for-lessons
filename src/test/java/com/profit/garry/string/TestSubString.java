package com.profit.garry.string;

import org.junit.Test;

/**
 * Created by igor on 10.06.17.
 */
public class TestSubString {

    @Test
    public void testingSubStringSearch() {

        SubString subString = new SubString();

        String str = "ABCgfhdjdCBA";
        String strS = "ABCgfhdjdABC";
        System.out.println(subString.subStringSearch(str));
        System.out.println(subString.subStringSearch(strS));
    }
}
