package com.profit.garry.regex_test;

import com.profit.garry.regex.UserNameValidator;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class UserNameValidatorTEST {

    private UserNameValidator userNameValidator;

    @BeforeClass
    public void initData() {
        userNameValidator = new UserNameValidator();
    }

    @DataProvider
    public Object[][] ValidUserNameProvider() {
        return new Object[][]{{new String[]{"alex34", "alex_2002", "alex-2002", "alex3-4_good"}}
        };
    }

    @DataProvider
    public Object[][] InvalidUserNameProvider() {
        return new Object[][]{{new String[]{"al", "al@eksey", "alekey123456789_-"}}
        };
    }

    @Test(dataProvider = "ValidUserNameProvider")
    public void ValidUsernameTest(String[] Username) {

        for (String temp : Username) {
            boolean valid = userNameValidator.validate(temp);
            System.out.println("Username is valid : " + temp + " " + valid);
            Assert.assertEquals(true, valid);

        }
    }

    @Test(dataProvider = "InvalidUserNameProvider",
            dependsOnMethods = "ValidUserNameProvider")
    public void InValidUserNameProvider(String[] Username) {

        for (String temp : Username) {
            boolean valid = userNameValidator.validate(temp);
            System.out.println("Username is valid : " + temp + " , " + valid);
            Assert.assertEquals(false, valid);

        }

    }
}
