package com.profit.garry.datatypes;

import org.junit.Test;

/**
 * Created by igor on 29.05.17.
 */
public class TestDataTypes {

    @Test
    public void testDataTypes() {

        DataTypes dataTypes = new DataTypes((byte) 5, (short) 765, 456,
                76543L, 3.45,
                3.56f, 'c', false);
        System.out.println(dataTypes.getaBoolean());
        System.out.println(dataTypes.getaByte());
        System.out.println(dataTypes.getaShort());
        System.out.println(dataTypes.getAnInt());
        System.out.println(dataTypes.getaLong());
        System.out.println(dataTypes.getaDouble());
        System.out.println(dataTypes.getaFloat());
        System.out.println(dataTypes.getaChar());

    }
}
