package com.profit.garry.generic_test;

import com.profit.garry.generic.Animal;
import com.profit.garry.generic.Dog;
import com.profit.garry.generic.GenericSmpl;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestGen {

    @Test
    public void test() {
        GenericSmpl<String> genericSmpl = new GenericSmpl<>();
        genericSmpl.test("Test");

        GenericSmpl<Integer> genericSmplInt = new GenericSmpl<>();
        genericSmplInt.test(5);
    }

    @Test
    public void test2() {

        List<Integer> list = new ArrayList<>();
        list.add(5);

        List<Dog> dogList = new ArrayList<>();
        dogList.add(new Dog().nameT("Sharik"));

        List<Animal> animals = new ArrayList<>();

        Animal animal = new Animal() {

            String name;

            @Override
            public Animal nameT(Object o) {
                this.name = (String) o;
                return this;
            }

            @Override
            public Object getData() {
                return this.name;
            }

            @Override
            public String toString() {
                return "$classname{" +
                        "name='" + name + '\'' +
                        '}';
            }
        };

        animal.nameT("Zhiraf");
        animals.add(animal);

        collection1(list);
        collection2(dogList);
        collection3(animals);

    }

    public void collection1(List<?> list) {
        list.forEach(System.out::println);
    }

    public void collection2(List<? extends Animal> list) {
        list.forEach(x -> System.out.println(x.getData()));
    }

    public void collection3(List<? super Dog> list) {
        list.forEach(x -> System.out.println(x.toString()));

    }
}
