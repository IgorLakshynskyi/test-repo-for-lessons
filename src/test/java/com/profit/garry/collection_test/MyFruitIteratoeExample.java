package com.profit.garry.collection_test;

import com.profit.garry.collection.FruitIterator;
import com.profit.garry.collection.MyOwnArrayList;
import org.junit.Test;

/**
 * Created by igor on 09.07.17.
 */
public class MyFruitIteratoeExample {

    @Test
    public void test() {
        MyOwnArrayList fruitList = new MyOwnArrayList();
        fruitList.add("Mango");
        fruitList.add("Strawberry");
        fruitList.add("Papaya");
        fruitList.add("Watermaleon");

        System.out.println("------Calling my iterator on my ArrayList-------");

        FruitIterator it = fruitList.iterator();
        while (it.hasNext()) {
            String s = (String) it.next();
            System.out.println("Value: " + s);
        }
        System.out.println("--Fruit List size: " + fruitList.size());
        fruitList.remove(1);
        System.out.println("--After removal, Fruit List size: " + fruitList.size());

        for (int i = 0; i < fruitList.size(); i++) {
            System.out.println(fruitList.get(i));
        }

    }
}
