package com.profit.garry.jaxb_test;

import com.profit.garry.jaxb.Duke;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Created by igor on 21.06.17.
 */
public class DukeTest {

    private JAXBContext context;

    @Before
    public void init() throws JAXBException {
        this.context = JAXBContext.newInstance(Duke.class);
    }

    @Test
    public void serialization() throws JAXBException {
        Marshaller marshaller = this.context.createMarshaller();
        marshaller.marshal(new Duke("java", 2), new File("duke.xml"));

        Unmarshaller unmarshaller = this.context.createUnmarshaller();
        Object unmarshal = unmarshaller.unmarshal(new File("duke.xml"));
        System.out.println("Unmarshal = " + unmarshal);


    }
}
