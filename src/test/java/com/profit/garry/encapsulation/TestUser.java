package com.profit.garry.encapsulation;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by igor on 17.06.17.
 */
public class TestUser {

    @Test
    public void testingPasswordNormal() {
        User u = new User();
        u.setPassword("qwerty");
        assertEquals("qwerty", u.getPassword());
    }

    @Test
    public void testingSetPasswordShort() {
        User u = new User();
        u.setPassword("qw");
        assertEquals(null, u.getPassword());
    }

    @Test
    public void testingSetPasswordNuul() {
        User u = new User();
        u.setPasswordNull(null);
        assertEquals(null, u.getPassword());
    }

    @Test(expected = NullPointerException.class)
    public void testingSetPasswordNullSecond() {
        User u = new User();
        u.setPassword(null);
        assertEquals(null, u.getPassword());
    }

    @Test
    public void testingSetLoginIDValue() {
        User u = new User();
        u.setLoginID("a123");
        assertEquals("a123", u.getLoginID());
    }

    @Test
    public void testSetLoginIDValid() {
        User u = new User();
        u.setLoginID("Login");
        assertEquals("Login", u.getLoginID());
    }

    @Test
    public void testSetLoginIDValidSecond() {
        User u = new User();
        u.setLoginIDSecond("@@@^$*#");
        assertEquals(null, u.getLoginID());
    }

}
