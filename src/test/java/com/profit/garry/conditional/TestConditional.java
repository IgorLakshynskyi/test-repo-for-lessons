package com.profit.garry.conditional;

import org.junit.Test;

import static com.profit.garry.conditional.ConditionalStatements.positivOrNegativ;

/**
 * Created by igor on 29.05.17.
 */
public class TestConditional {

    @Test
    public void testPositivOrNo() {
        positivOrNegativ(45);
        positivOrNegativ(-23);
        positivOrNegativ(0);
    }
}
