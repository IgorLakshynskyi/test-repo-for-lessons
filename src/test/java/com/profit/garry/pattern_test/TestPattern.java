package com.profit.garry.pattern_test;

import com.profit.garry.pattern.abstraktFactory.AbstraktFactory;
import com.profit.garry.pattern.abstraktFactory.SpeciesFactory;
import com.profit.garry.pattern.builder.*;
import com.profit.garry.pattern.factory.Animal;
import com.profit.garry.pattern.factory.AnimalFactory;
import com.profit.garry.pattern.prototype.Dog;
import com.profit.garry.pattern.prototype.Person;
import com.profit.garry.pattern.singltone.SingletonExample;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by igor on 15.07.17.
 */
public class TestPattern {

    @Test
    public void builder() {
        Student s = new Student.Builder().name("Igor").age(33).language(Arrays.asList("ivrit", "ukraine")).build();
        System.out.println(s);
    }

    @Test
    public void singleton() {
        SingletonExample singletonExample = SingletonExample.getInstance();
        singletonExample.sayHello();
    }

    @Test
    public void factory() {
        AnimalFactory animalFactory = new AnimalFactory();

        Animal a1 = animalFactory.getAnimals("feline");
        System.out.println("a1 sound: " + a1.makeSound());

        Animal a2 = animalFactory.getAnimals("canine");
        System.out.println("a2 sound: " + a2.makeSound());

    }

    @Test
    public void abstractFactory() {
        AbstraktFactory abstraktFactory = new AbstraktFactory();

        SpeciesFactory speciesFactory1 = abstraktFactory.getSpeciesFactory("reptile");
        Animal a1 = speciesFactory1.getAnimal("tyrannosaurus");
        System.out.println("a1 sound: " + a1.makeSound());
        Animal a2 = speciesFactory1.getAnimal("snake");
        System.out.println("a2 sound: " + a2.makeSound());

        Animal animal = abstraktFactory.getSpeciesFactory("reptile").getAnimal("snake");
        System.out.println("reptile sound: " + animal.makeSound());

        SpeciesFactory speciesFactory2 = abstraktFactory.getSpeciesFactory("mammal");
        Animal a3 = speciesFactory2.getAnimal("dog");
        System.out.println("a3 sound: " + a3.makeSound());

        Animal a4 = speciesFactory2.getAnimal("cat");
        System.out.println("a4 sound: " + a4.makeSound());
    }

    @Test
    public void prototype() {
        Person person1 = new Person("Fred");
        System.out.println("person 1: " + person1);

        Person person2 = (Person) person1.doClone();
        System.out.println("person 2: " + person2);

        Dog dog1 = new Dog("Woof!");
        System.out.println("dog 1: " + dog1);

        Dog dog2 = (Dog) dog1.doClone();
        System.out.println("dog 2: " + dog2);
    }

    @Test
    public void builder2() {

        MealBuilder mealBuilder = new JapaneseMealBuilder();
        MealDirector mealDirector = new MealDirector(mealBuilder);
        mealDirector.constructMeal();
        Meal meal = mealDirector.getMeal();
        System.out.println("meal is: " + meal);

        Meal x = new MealDirector(new JapaneseMealBuilder()).constructMeal().getMeal();
        System.out.println("Meal x: " + x);
    }
}
