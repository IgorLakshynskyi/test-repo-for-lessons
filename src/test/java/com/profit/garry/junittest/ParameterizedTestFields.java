package com.profit.garry.junittest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;


/**
 * Created by igor on 25.06.17.
 */

@RunWith(Parameterized.class)
public class ParameterizedTestFields {

    @Parameterized.Parameter(0)
    public int mI;
    @Parameterized.Parameter(1)
    public int mS;
    @Parameterized.Parameter(2)
    public int result;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{{1, 2, 2}, {5, 3, 15}, {121, 4, 484}};
        return Arrays.asList(data);

    }

    @Test
    public void testMultiplyExceptoin() {
        Calculator calculator = new Calculator();
        assertEquals("Result", result, calculator.multiply(mI, mS));
    }
}
