package com.profit.garry.junittest;

import org.junit.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by igor on 25.06.17.
 */
public class CalculatorTestSecond {

    @BeforeClass //it's @BeforeAll
    public static void setUpBeforeClass() throws Exception {
        System.out.println("before class");
    }

    @Before // it's @BeforeEach
    public void setUp() throws Exception {
        System.out.println("before");
    }

    @Test
    public void testFindMax() {
        System.out.println("test case find max");
        assertEquals(4, Calculator.findMax(new int[]{1, 2, 3, 4}));
        assertEquals(-2, Calculator.findMax(new int[]{-12, -3, -4, -2}));
    }

    @Test
    public void testCube() {
        System.out.println("test case cube");
        assertEquals(27, Calculator.cube(3));
    }

    @Test
    public void testReverseWord() {
        System.out.println("test case reverse Word");
        assertEquals("ym eman si nahk ", Calculator.reverseWord("my name is khan"));
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("after");
    }

    @AfterClass
    public static void tearDownAftherClass() throws Exception {
        System.out.println("After Class");
    }
}
