package com.profit.garry.junittest;

import com.profit.garry.firstapp.FirstApp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by igor on 25.06.17.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({CalculatorTest.class, CalculatorTestSecond.class})
public class AllTest {
}
