package com.profit.garry.junittest;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by igor on 25.06.17.
 */
public class CalculatorTest {

    @Test
    public void testFindMax() {
        assertEquals(4, Calculator.findMax(new int[]{1, 2, 3, 4}));
        assertEquals(-1, Calculator.findMax(new int[]{-12, -1, -3, -4, -2}));
    }

    @Test
    public void multiplicationOfZeroIntegersShouldReturnZero() {
        Calculator calculator = new Calculator();

        assertEquals("10 x 0 must be 0", 0, calculator.multiply(10, 0));
        assertEquals("0 x 10 must be 0", 0, calculator.multiply(0, 10));
        assertEquals("0 x 0 must be 0", 0, calculator.multiply(0, 0));
    }
}
