package com.profit.garry.junittest;

import com.ibm.icu.impl.Assert;
import org.junit.jupiter.api.*;

import java.util.function.Supplier;

/**
 * Created by igor on 25.06.17.
 */
public class AppTest {

    @BeforeAll
    static void setup() {
        System.out.println("@BeforeAll executed");
    }

    @BeforeEach
    void setupThis() {
        System.out.println("@BeforeForEach executed");
    }

    @Tag("DEV")
    @Test
    void testCalcOne() {
        System.out.println("===========TEST ONE EXECUTED=============");
        Assertions.assertEquals(4, Calculator.sum(2, 2));
    }

    @Tag("PROD")
    @Disabled
    @Test
    void testCalcTwo() {
        System.out.println("===========TEST TWO EXECUTED=============");
        Assertions.assertEquals(4, Calculator.sum(2, 4));
    }

    @AfterEach
    void tearThis() {
        System.out.println("AfterEach executed");
    }

    @AfterAll
    static void tear() {
        System.out.println("AfterAll executed");
    }

    @Tag("DEV")
    @Test
    void testCase() {
        Assertions.assertNotEquals(3, Calculator.sum(2, 2));

        Assertions.assertNotEquals(3, Calculator.sum(2, 2), "CalculatorTest.summ(2, 2) test failed");

        Supplier<String> messageSupplier = () -> "CalculatorTest.summ(2, 2) test failed";
        Assertions.assertNotEquals(3, Calculator.sum(2, 2), messageSupplier);
    }
}
