package com.profit.garry.junittest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by igor on 25.06.17.
 */

@RunWith(Parameterized.class)
public class ParameterizedTestFieldsSecond {

    private int mI;
    private int mS;

    public ParameterizedTestFieldsSecond(int mI, int mS) {
        this.mI = mI;
        this.mS = mS;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{{1, 2}, {5, 3}, {121, 4}};
        return Arrays.asList(data);
    }

    @Test
    public void testMultyException() {
        Calculator tester = new Calculator();
        assertEquals("Result", mI, mS, tester.multiply(mI, mS));
    }
}
