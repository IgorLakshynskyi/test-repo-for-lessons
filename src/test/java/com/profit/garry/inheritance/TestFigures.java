package com.profit.garry.inheritance;

import org.junit.Test;

/**
 * Created by igor on 11.06.17.
 */
public class TestFigures {

    @Test
    public void testingGetArea() {

        Circle circle = new Circle();
        System.out.println(circle.getArea());

        Circle circleS = new Circle(4.2);
        System.out.println(circleS.getRadius());
        System.out.println(circle.toString());

    }

    @Test
    public void testingCylinder() {

        Cylinder cylinder = new Cylinder();

        System.out.println(cylinder.getHeight());
        System.out.println(cylinder.getVolume());
        System.out.println(cylinder.getArea());
        System.out.println(cylinder.getRadius());
        System.out.println(cylinder.toString());

        System.out.println();

        Cylinder cylinderI = new Cylinder(4.5, 6.7);
        System.out.println(cylinderI.getRadius());
        System.out.println(cylinderI.getHeight());
        System.out.println(cylinderI.getArea());
        System.out.println(cylinderI.getVolume());
        System.out.println(cylinderI.toString());

        System.out.println();

        Cylinder cylinderS = new Cylinder(3.4);
        System.out.println(cylinderS.getHeight());
        System.out.println(cylinderS.getVolume());
        System.out.println(cylinderS.toString());


    }
}
