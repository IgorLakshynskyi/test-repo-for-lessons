package com.profit.garry.firstapp;

import org.junit.Test;

import static com.profit.garry.firstapp.FirstApp.print;
import static com.profit.garry.firstapp.FirstApp.println;

/**
 * Created by igor on 29.05.17.
 */
public class FirstAppTest {

    @Test
    public void firstAppTesting(){
        print("Hello!");
        println();
        println("Object");
    }
}
