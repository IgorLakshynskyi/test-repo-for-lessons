package com.profit.garry.loopsandarraystest;

import org.junit.Test;

import static com.profit.garry.loopsandarrays.GnomeSort.gnomeSort;

/**
 * Created by igor on 08.06.17.
 */
public class TestGnomeSort {

    @Test
    public void testingGnomeSort() {
        int[] arr = {2, 34, 51, 6, 7, 89, 14, 23, 4, 1, 5};
        gnomeSort(arr);

        for (Integer i : arr) {
            System.out.print(i + " ");

        }
    }
}
