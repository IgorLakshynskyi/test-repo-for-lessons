package com.profit.garry.loopsandarraystest;

import org.junit.Test;

import static com.profit.garry.loopsandarrays.BinarySearch.binarySearch;

/**
 * Created by igor on 08.06.17.
 */
public class TestBinarySearch {

    @Test
    public  void testingBinarySearch() {

        int[] arr = {1,2,3,4,5,6,7,8,9,10};
        binarySearch(arr, 12);
        binarySearch(arr, 6);

    }
}
