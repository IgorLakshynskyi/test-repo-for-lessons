package com.profit.garry.loopsandarraystest;

import com.profit.garry.loopsandarrays.SequentialSearch;
import org.junit.Test;

import static com.profit.garry.loopsandarrays.SequentialSearch.sequentialSearch;


/**
 * Created by igor on 08.06.17.
 */
public class TestSequentialSearch {

    @Test
    public void testingContains() {
        int[] arr = {2,43,5,7,8,554,3,22,15,6,53,0};
        sequentialSearch(arr, 0);
        sequentialSearch(arr, 32);
    }
}
