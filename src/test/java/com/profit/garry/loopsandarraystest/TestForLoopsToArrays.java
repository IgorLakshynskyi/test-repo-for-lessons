package com.profit.garry.loopsandarraystest;

import org.junit.Test;

import static com.profit.garry.loopsandarrays.ForLoopsToArrays.equalityOfTwoArrays;

import static com.profit.garry.loopsandarrays.ForLoopsToArrays.uniqueArray;

import static com.profit.garry.firstapp.FirstApp.*;

/**
 * Created by igor on 03.06.17.
 */
public class TestForLoopsToArrays {

    @Test
    public void testingArraysToEaquls() {

        int[] a = {2,3,4,5,6};
        int[] b = {2,3,4,5,6};
        int[] c = {2,7,4,5,6};
        equalityOfTwoArrays(a,b);
        equalityOfTwoArrays(a,c);
    }

    @Test
    public void testingUniqueArray() {

        int[] x = {2,3,4,3};
        int[] y = {2,3,4,5, 3};

        uniqueArray(x);
        println();
        uniqueArray(y);

    }
}
