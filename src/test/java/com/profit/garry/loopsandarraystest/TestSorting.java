package com.profit.garry.loopsandarraystest;

import org.junit.Test;

import java.util.Arrays;

import static com.profit.garry.loopsandarrays.Sorting.selectionSort;

/**
 * Created by igor on 04.06.17.
 */
public class TestSorting {

    @Test
    public void testingSortinga() {

        int[] array = {2, 1, 4, 3};
        selectionSort(array);

//        for (int i = 0; i < array.length; i++) {
//            System.out.print(array[i] + " ");
//        }

        System.out.print(Arrays.toString(array) + " ");
    }
}
