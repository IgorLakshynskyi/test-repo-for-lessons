package com.profit.garry.loopsandarraystest;

import org.junit.Test;

import static com.profit.garry.loopsandarrays.Matrix.multiplicar;

/**
 * Created by igor on 03.06.17.
 */
public class TestMatrix {

    @Test
    public void testingMatrix() {

        Double[][] a = {{2.4, 3.5, 4.2},
                {6.3, 5.4, 4.5},
                {6.3, 5.4, 4.5}};

        Double[][] b = {{5.3, 4.4, 7.1},
                {2.2, 3.3, 8.4},
                {6.3, 5.4, 4.5}};

        Double[][] result = multiplicar(a, b);

        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result.length; j++) {
                System.out.print(result[i][j] + " ");
            }
            System.out.println();
        }
    }
}
