package com.profit.garry.javaeight;

import org.junit.Test;

import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Map.Entry.comparingByKey;

public class StreamFilterTest {

    @Test
    public void filter() {

        List<String> lines = Arrays.asList("first", "second", "third");
        List<String> result = lines.stream().filter(line -> !"second".equals(line)).collect(Collectors.toList());

        result.forEach(System.out::println);
    }

    @Test
    public void filterSecond() {

        List<Person> persons = Arrays.asList(
                new Person(1, "first", 30),
                new Person(2, "second", 20),
                new Person(3, "third", 40)
        );

        Person result1 = persons.stream().filter(x -> x.getId().equals(2)).findAny().orElse(null);
        System.out.println(result1);

        Person result2 = persons.stream().filter(x -> x.getId().equals(5)).findAny().orElse(null);
        System.out.println(result2);
    }

    @Test
    public void filterAndMap() {

        List<Person> persons = Arrays.asList(
                new Person(1, "first", 30),
                new Person(2, "second", 20),
                new Person(3, "third", 40)
        );

        String name = persons.stream().filter(x -> "second".equals(x.getName()))
                .map(Person::getName).findAny().orElse("");

        System.out.println("name : " + name);

        List<String> collect = persons.stream()
                .map(Person::getName)
                .collect(Collectors.toList());

        collect.forEach(System.out::println);


        Integer id = persons.stream().filter(x -> x.getId().equals(3))
                .map(Person::getId)
                .findAny().orElse(null);

        System.out.println("id : " + id);

        Person person = persons.stream().filter(x -> x.getId().equals(id))
                .findAny().orElse(null);

        System.out.println(person);

        List<Integer> collected = persons.stream().map(Person::getId).collect(Collectors.toList());
        collect.forEach(System.out::println);

    }

    @Test
    public void ownerTest() {

        List<Person> persons = Arrays.asList(
                new Person(1, "first", 30),
                new Person(2, "second", 20),
                new Person(3, "third", 40)
        );
//        Person person = persons.stream().filter(x -> x.getId().equals(1))
//                .findAny().orElse(null);
//        Person person2 = persons.stream().filter(x -> x.getId().equals(2))
//                .findAny().orElse(null);
//        Person person3 = persons.stream().filter(x -> x.getId().equals(3))
//                .findAny().orElse(null);
//
//        System.out.println(person);
//        System.out.println(person2);
//        System.out.println(person3);

//
//        List<Person> secondListt = persons.stream().collect(Collectors.toList());
//        secondListt.forEach(System.out::println);

//        List<String> secondList = persons.stream()
//                .map(Person::toString)
//                .collect(Collectors.toList());
//
//        secondList.forEach(System.out::println);

        persons.forEach(System.out::println);
    }

    @Test
    public void map() {

        List<String> alpha = Arrays.asList("aa", "bb", "cc", "dd");

        List<String> alphaUpper = new ArrayList<>();
        for (String s : alpha) {
            alphaUpper.add(s.toUpperCase());
        }

        System.out.println(alpha);
        System.out.println(alphaUpper);

        List<String> collect = alpha.stream().map(String::toUpperCase).collect(Collectors.toList());
        collect.forEach(x -> {
            System.out.print(x + " ");
        });
        System.out.println();


        List<Integer> num = Arrays.asList(1, 2, 3, 4, 5);
        List<Integer> collectSecond = num.stream().map(n -> n * 2).collect(Collectors.toList());
        System.out.println(collectSecond);
    }

    @Test
    public void mapObject() {

        List<Person> staff = Arrays.asList(
                new Person(1, "first", 30),
                new Person(2, "second", 20),
                new Person(3, "third", 40)
        );
        // Before java8
        List<String> result = new ArrayList<>();
        for (Person x : staff) {
            result.add(x.getName());
        }
        System.out.println(result);

        //Java8
        List<String> collect = staff.stream().map(x -> x.getName()).collect(Collectors.toList());
        System.out.println(collect);

        List<Person> result2 = staff.stream().map(tamp -> {
            Person obj = new Person();
            obj.setName(tamp.getName());
            obj.setAge(tamp.getAge());
            obj.setId(tamp.getId());
            return obj;
        }).collect(Collectors.toList());

        System.out.println(result2);
    }

    @Test
    public void groupingBy() {

        List<String> items = Arrays.asList("apple", "apple", "banana", "apple", "orange", "banana", "papaya");

        Map<String, Long> result =
                items.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        System.out.println(result);

        Map<String, Long> finalMap = new LinkedHashMap<>();

        result.entrySet().stream().sorted(Map.Entry.<String, Long>comparingByKey().reversed()).forEachOrdered(e -> finalMap.put(e.getKey(), e.getValue()));

        System.out.println(finalMap);


    }

    @Test
    public void finalNull() {

        Supplier<Stream<String>> languageSupplier = () -> Stream.of("java", "node", null, "ruby", null, "php", "phyton");

        List<String> result = languageSupplier.get().filter(x -> x != null).collect(Collectors.toList());
        result.forEach(System.out::println);

        System.out.println("--------------------");

        List<String> resultNew = languageSupplier.get().filter(Objects::nonNull).collect(Collectors.toList());
        resultNew.forEach(System.out::println);
    }

    @Test
    public void arrayToStream() {
        String[] array = {"a", "b", "c", "d", "e"};

        Stream<String> stream1 = Arrays.stream(array);
        stream1.forEach(System.out::println);

        Stream<String> stream2 = Stream.of(array);
        stream2.forEach(System.out::println);
    }

    @Test
    public void primitiveArrayStream() {

        int[] intArray = {1, 2, 3, 4, 5};

        IntStream intStream = Arrays.stream(intArray);
        intStream.forEach(System.out::println);

        Stream<int[]> temp = Stream.of(intArray);

        IntStream intStream2 = temp.flatMapToInt(x -> Arrays.stream(x));
        intStream2.forEach(System.out::println);
    }

    @Test
    public void convertStreamToList() {

        Stream<String> language = Stream.of("java", "python", "node");

        List<String> result = language.collect(Collectors.toList());
        result.forEach(System.out::println);

        Stream<Integer> numbers = Stream.of(1, 2, 3, 4, 5);

        List<Integer> result2 = numbers.filter(x -> x != 3).collect(Collectors.toList());

        result2.forEach(System.out::println);


        List<Person> stuff = Arrays.asList(
                new Person(1, "first", 30),
                new Person(2, "second", 20),
                new Person(3, "third", 40)
        );

        Map<Integer, String> map = new HashMap<>();

        map = mapObj(stuff);

        map.forEach((x, y) -> {
            System.out.println(x + " " + y);
        });

        map = stuff.stream().collect(Collectors.toMap(Person::getId, Person::getName));

        map.forEach((x, y) -> {
            System.out.println(x + " " + y);
        });
    }

    /**
     * @param personList
     * @return
     */
    public HashMap<Integer, String> mapObj(List<Person> personList) {

        final HashMap<Integer, String> map = new HashMap<>();
        personList.forEach((x) -> {
            map.put(x.getId(), x.getName());
        });
        return map;
    }


}
