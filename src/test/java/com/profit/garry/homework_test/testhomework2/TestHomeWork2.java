package com.profit.garry.homework_test.testhomework2;

import org.junit.Test;

import static com.profit.garry.homework.homework2.HomeWork2.*;

/**
 * Created by igor on 01.06.17.
 */
public class TestHomeWork2 {

    @Test
    public void testingTask1() {
        bigger(4, 67);
        bigger(432, 67, 34);
        bigger(4, 67, 34, 98);
    }

    @Test
    public void testingTask2() {
        taskTwo(2, 5);
        taskTwo(4, 3);
        taskTwo(5, 5);
    }

    @Test
    public void testing3() {
        sortTaskThree(5, 3);
        sortTaskThree(32, 3, 45);
        sortTaskThree(1, 8, 6, 4);
    }
}
