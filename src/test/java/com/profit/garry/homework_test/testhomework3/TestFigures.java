package com.profit.garry.homework_test.testhomework3;

import com.profit.garry.homework.homework3.*;
import org.junit.Test;

/**
 * Created by igor on 13.06.17.
 */
public class TestFigures {

    @Test
    public void testingFigures() {

        Square square = new Square(2.0, 4.0);
        Square cube = new Cube(3.0, 2.0);
        Square parallelogram = new Parallelogram(2.2, 4.4, 5);

        System.out.println("Square: " + square.toString());
        System.out.println("Square's area is " + square.getArea());
        System.out.println("Square's perimeter is " + square.calculatorPerimeter());

        System.out.println();

        System.out.println(cube.toString());
        System.out.println("Cube's area is " + cube.calculatorArea());
        System.out.println("Cube's perimeter is " + cube.calculatorPerimeter());

        System.out.println();

        System.out.println("Parallelogram: " + parallelogram.toString());
        System.out.println("Parallelogram's area is " + parallelogram.getArea());
        System.out.println("Parallelogram's perimetr is " + parallelogram.getPerimeter());
        System.out.println();

        Circle circle = new Circle(3.4);
        Circle ball = new Ball(2.9);

        System.out.println("Circle: " + circle.toString());
        System.out.println("Circle's area is " + circle.getArea());
        System.out.println("Circle's perimeter is " + circle.getPerimeter());
        System.out.println();

        System.out.println("Ball: " + ball.toString());
        System.out.println("Ball's area is " + ball.getArea());
        System.out.println("Ball's perimeter is " + ball.getPerimeter());
        System.out.println();

        Triangle triangle = new Triangle(3,4,5,6,7);
        Triangle trapezium = new Trapezium(2, 3, 4, 5, 6, 7);

        System.out.println("Triangle: " + triangle.toString());
        System.out.println("Triangle's area is  " + triangle.getArea());
        System.out.println("Triangle's perimeter is " + triangle.getPerimeter());
        System.out.println();

        System.out.println(trapezium.toString());
        System.out.println(trapezium.getArea());
        System.out.println(trapezium.getPerimeter());

    }
}
