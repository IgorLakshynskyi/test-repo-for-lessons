package com.profit.garry.homework_test.homeworkxmltest;

import org.junit.Test;

import static com.profit.garry.homework.homeworkxml.DOMxml.lineXml;
import static com.profit.garry.homework.homeworkxml.DOMxml.newFileWOTags;
import static com.profit.garry.homework.homeworkxml.DOMxml.replaceTags;

/**
 * Created by igor on 20.06.17.
 */
public class DOMxmlTest {

        @Test
        public void stringBldrFromXml() {
            String filename = "C:\\Users\\Admin\\IdeaProjects\\apollo\\src\\main\\resources\\xml\\xmlHW.xml";
            System.out.println(lineXml(filename));
            newFileWOTags(lineXml(filename));
        }

        @Test
        public void replaceAllTags() {
            String filename = "C:\\Users\\Admin\\IdeaProjects\\apollo\\src\\main\\resources\\xml\\xmlHW.xml";
            replaceTags(lineXml(filename));
        }
}
