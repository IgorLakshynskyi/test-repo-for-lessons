package com.profit.garry.inner_test;

import com.profit.garry.inner.Cache;
import com.profit.garry.inner.MyInterface;
import com.profit.garry.inner.Outer;
import org.junit.Test;

/**
 * Created by igor on 01.07.17.
 */
public class OuterTest {
    @Test
    public void outerTest() {

        Outer.Nested nestedI = new Outer.Nested();

        nestedI.printNestedText();

        Outer outer = new Outer();
        Outer.Inner inner = outer.new Inner();

        inner.printText();

        outer.doIT();

        Outer outerI = new Outer() {
            @Override
            public void doIT() {
                System.out.println("Anonymous class doIT()");
            }
        };
        outerI.doIT();
    }

    @Test
    public void myIntTest() {
        final String textToPrint = "Text...";

        MyInterface myInterface = new MyInterface() {

            private String text;
            {
                this.text = textToPrint;
            }

            @Override
            public void doIT() {
                System.out.println("Anonymys class doIT");
                System.out.println(this.text);

            }
        };
        myInterface.doIT();
    }

    @Test
    public void cacheTest() {

        String key = "Password";
        Integer pass = 12345678;

        Cache cache = new Cache();
        cache.store(key,pass);

        System.out.println(cache.get("Password"));
        System.out.println(cache.getData("Password"));
    }

    @Test
    public void local() {
        LocalInner obj = new LocalInner();
        obj.display();
    }
}
