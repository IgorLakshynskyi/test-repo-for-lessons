package com.profit.garry.inner_test;

import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.junit.*;
import org.junit.runner.RunWith;

/**
 * Created by igor on 01.07.17.
 */
@RunWith(HierarchicalContextRunner.class)
public class NestedTest {

    @BeforeClass
    public static void beforeAllTestMethod() {
        System.out.println("Invoked once before all test methods");
    }

    @Before
    public void beforeEachTestMethod() {
        System.out.println("Invoked before each test methods");
    }

    @After
    public void afterEachTestMethod() {
        System.out.println("Invoked after each test methods");
    }

    @AfterClass
    public static void afterAllTestMethod() {
        System.out.println("Invoked onece after all test methods");
    }

    @Test
    public void rootClassTest() {
        System.out.println("Root class test");
    }

    public class ContextA {

        @Before
        public void beforEachTestMethodContextA() {
            System.out.println("Invoked before each test methods context A");
        }

        @After
        public void afterEachTestMethodContextA() {
            System.out.println("Invoked after each test methods contex A");
        }

        @Test
        public void contextATest() {
            System.out.println("Context A Test");
        }

        public class ContextC {
            @Before
            public void beforEachTestMethodContextC() {
                System.out.println("Invoked before each test methods context C");
            }

            @After
            public void afterEachTestMethodContextC() {
                System.out.println("Invoked after each test methods contex C");
            }

            @Test
            public void contextCTest() {
                System.out.println("Context C Test");
            }
        }
    }
}
