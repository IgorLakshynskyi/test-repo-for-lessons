package com.profit.garry.inner_test;

/**
 * Created by igor on 01.07.17.
 */
public class LocalInner {

    private String message = "igor.com";

    void display() {
        final int data = 20;

        class Local {
            void msg() {
                System.out.println(message + " : " + data);
            }
        }

        Local loc = new Local();
        loc.msg();
    }
}
