package com.profit.garry.interfaces;

import org.junit.Test;

import java.util.Arrays;

/**
 * Created by igor on 18.06.17.
 */
public class SortFruitObgect {

    @Test
    public void sortFruitObgects() {

        Fruit[] fruits = new Fruit[4];

        Fruit pineapple = new Fruit("Pineapple", "Pineapple description", 70);
        Fruit apple = new Fruit("Apple", "Apple description", 100);
        Fruit orange = new Fruit("Orange", "Orange description", 80);
        Fruit banana = new Fruit("Banana", "Banana description", 90);

        fruits[0] = pineapple;
        fruits[1] = apple;
        fruits[2] = orange;
        fruits[3] = banana;

//        Arrays.sort(fruits);
        Arrays.sort(fruits, Fruit.fruitNameComparator);

        int i = 0;
        for (Fruit temp : fruits) {
            System.out.println("fruits " + ++i + " : " + temp.getFruitName()
                    + ", Quantity : " + temp.getQuantity());

        }

    }
}
