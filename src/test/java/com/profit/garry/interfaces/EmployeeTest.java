package com.profit.garry.interfaces;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by igor on 18.06.17.
 */
public class EmployeeTest {

    @Test
    public void testEmployeeSorting() {

        Employee e1 = new Employee(1, "A", 1000, 32, new Date(2011, 10, 1));
        Employee e2 = new Employee(2, "AB", 1300, 22, new Date(2012, 10, 1));
        Employee e3 = new Employee(3, "B", 10, 42, new Date(2010, 11, 3));
        Employee e4 = new Employee(4, "CD", 100, 23, new Date(2011, 10, 1));
        Employee e5 = new Employee(5, "AAA", 1200, 26, new Date(2011, 10, 1));

        List<Employee> listEmployees = new ArrayList<>();
        listEmployees.add(e1);
        listEmployees.add(e2);
        listEmployees.add(e3);
        listEmployees.add(e4);
        listEmployees.add(e5);
        System.out.println("Unsorted List : " + listEmployees);

        Collections.sort(listEmployees);
        assertEquals(e1, listEmployees.get(0));
        assertEquals(e5, listEmployees.get(4));
        System.out.println("Sorting by natural order : " + listEmployees);


        Collections.sort(listEmployees, Employee.nameComparator);
        assertEquals(e1, listEmployees.get(0));
        assertEquals(e4, listEmployees.get(4));
        System.out.println("Sorting by name order : " + listEmployees);


        Collections.sort(listEmployees, Employee.ageComparator);
        assertEquals(e2, listEmployees.get(0));
        assertEquals(e3, listEmployees.get(4));
        System.out.println("Sorting by age order : " + listEmployees);


        Collections.sort(listEmployees, Employee.salaryComparator);
        assertEquals(e3, listEmployees.get(0));
        assertEquals(e2, listEmployees.get(4));
        System.out.println("Sorting by salary order : " + listEmployees);


        Collections.sort(listEmployees, Employee.dateOfJoiningComparator);
        assertEquals(e3, listEmployees.get(0));
        assertEquals(e2, listEmployees.get(4));
        System.out.println("Sorting by date order : " + listEmployees);
    }
}
